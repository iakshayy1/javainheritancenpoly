    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.util.Date;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class WashingMachine extends HouseholdDevices{
    private String type;

    public WashingMachine(String type, double watts, double height, double width, double length, double cost, Date year, String make) {
        super(watts, height, width, length, cost, year, make);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString()+" WashingMachine{" + "type = " + type + '}';
    }
}
