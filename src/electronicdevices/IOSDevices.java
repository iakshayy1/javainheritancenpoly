/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.util.Date;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class IOSDevices extends Gadgets {
    private boolean has3DTouch;

    public IOSDevices(boolean has3DTouch, double battery, double screenSize, double cost, Date year, String make) {
        super(battery, screenSize, cost, year, make);
        this.has3DTouch = has3DTouch;
    }

    public boolean isHas3DTouch() {
        return has3DTouch;
    }

    public void setHas3DTouch(boolean has3DTouch) {
        this.has3DTouch = has3DTouch;
    }
    
    public String getModelName(){
        if(super.getScreenSize()>4.7)
        {
            return "iPhone 7 Plus";
        }
        else
        {
            return "iPhone 7";
        }
    }

    @Override
    public String toString() {
        return super.toString()+" IOSDevices{" + "has3DTouch = " + has3DTouch + '}';
    }
    
    
    
    
    
    

   
    
    
}
