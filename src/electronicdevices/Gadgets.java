/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.text.ParseException;
import java.util.Date;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Gadgets extends ElectronicDevices{
       private double battery;
       private double screenSize;
       
       
       public Gadgets(double battery, double screenSize) throws ParseException {
        this.battery = battery;
        this.screenSize = screenSize;
    }
       
    public Gadgets(double battery, double screenSize, double cost, Date year, String make) {
        super(cost, year, make);
        this.battery = battery;
        this.screenSize = screenSize;
    }

    public double getBattery() {
        return battery;
    }

    public double getScreenSize() {
        return screenSize;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }

   public String batteryLife(){
       if(battery>5000)
       {
           return "Has More Battery Life";
       }
       else
       {
           return "Has less Battery Life";
       }
   }
   
       @Override
   public String expensiveOrNot(){
       if(super.getCost()>1000)
       {
           return "This Gadget is Expensive";
       }
       else
       {
           return "This Gadget is Inexpensive";
       }
   }

    @Override
    public String toString() {
        return super.toString()+" Gadgets{" + "battery= " + battery + ", screenSize= " + screenSize + '}';
    }
}

   
   
       
       
       
       

    

