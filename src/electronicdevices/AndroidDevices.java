/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.util.Date;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class AndroidDevices extends Gadgets{
    private double version;


    public AndroidDevices(double version, double battery, double screenSize, double cost, Date year, String make) {
        super(battery, screenSize, cost, year, make);
        this.version = version;
    }

    public String getVersion() {
        if(version<5.0){
            return "Very old Android Device";
        }
        else{
            return "Latest version";
        }
    }

    public void setVersion(double version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return super.toString()+" AndroidDevices{" + "version= " + version + '}';
    }
    
 }
