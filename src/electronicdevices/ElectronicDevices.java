/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class ElectronicDevices {
    private double cost;
    private Date year;
    private String make;

    public ElectronicDevices() throws ParseException {
        this.cost = 0.0d;
        String Date1 = "2004Jan01";
        DateFormat d1 = new SimpleDateFormat("yyyyMMMdd",Locale.ENGLISH);
        Date result1 =  d1.parse(Date1);
        this.year = result1;
    }

    public ElectronicDevices(double cost, Date year, String make) {
        this.cost = cost;
        this.year = year;
        this.make = make;
    }

    public double getCost() {
        return cost;
    }

    public Date getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public void setMake(String make) {
        this.make = make;
    }
    
    public String expensiveOrNot()
    {
        if(cost>500)
        {
            return  "Device is Expensive";
        }
        else
        {
            return "Device is Inexpensive";
        }
    } 

    @Override
    public String toString() {
        return "ElectronicDevices{" + "cost= " + cost + ", year= " + year + ", make= " + make + '}';
    }
    
}
