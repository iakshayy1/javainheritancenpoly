/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.util.Date;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class HouseholdDevices extends ElectronicDevices{
     private double watts;
     private double height;
     private double width;
     private double length;


    public HouseholdDevices(double watts, double height, double width, double length, double cost, Date year, String make) {
        super(cost, year, make);
        this.watts = watts;
        this.height = height;
        this.width = width;
        this.length = length;
    }

   
    public double getWatts() {
        return watts;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWatts(double watts) {
        this.watts = watts;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }
     
    public double getDeviceArea()
    {
        return height*width*length;
    }
    
    public double powerUsedInKWs(double hoursUsed)
    {
        return hoursUsed*watts/1000;
    }
     
     @Override
    public String expensiveOrNot()
    {
     if(super.getCost()>1500)
     {
         return "This Household Device is Expensive";         
     }
     else
     {
         return "This Household Device is Inexpensive";
     }
    }

    @Override
    public String toString() {
        return super.toString()+" HouseholdDevices{" + "watts= " + watts + ", height= " + height + ", width= " + width + ", length= " + length + '}';
    }
    
 }
