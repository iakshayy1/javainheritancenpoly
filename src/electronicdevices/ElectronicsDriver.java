/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicdevices;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class ElectronicsDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, ParseException {
        // TODO code application logic here
        ArrayList<ElectronicDevices> ed = new ArrayList<ElectronicDevices>();
        int i=1;
        Scanner sc = new Scanner(new File("input.txt"));
        while(sc.hasNextLine())
        {
            
            String a = sc.nextLine();
            if(a.equalsIgnoreCase("electronic device"))
              {
                double cst = Double.parseDouble(sc.nextLine());
                String dtr = sc.nextLine();
                DateFormat d = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                Date result =  d.parse(dtr); 
                String make = sc.nextLine();
                ElectronicDevices Ed = new ElectronicDevices(cst,result,make);
                System.out.println("Device "+i+": "+Ed.toString());
                ed.add(Ed);
                i++;
              }
                
            
                
            else if(a.equalsIgnoreCase("gadget"))
                {
                    String h = sc.nextLine();
                    double cst1= Double.parseDouble(h);
                    String Date1 = sc.nextLine();
                    DateFormat d1 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result1 =  d1.parse(Date1);
                    String make1 = sc.nextLine();
                    String bty = sc.nextLine();
                    double btry = Double.parseDouble(bty);
                    String scr = sc.nextLine();
                    double scrnsz = Double.parseDouble(scr);
                    ElectronicDevices Ed1 = new Gadgets(btry,scrnsz,cst1,result1,make1);
                   
                    System.out.println("Device "+i+": "+Ed1.expensiveOrNot());
                    System.out.println("a.The method in Gadgets is called.because the override methods engaged with run-time determination of which method to be invoked is referred as late binding polymorphism.  ");
                    Gadgets gd2 = (Gadgets)Ed1;
                    System.out.println("Device "+i+": "+gd2.expensiveOrNot());
                    System.out.println("b. The object of gadgets refer to the Ed1 object of gadgets which is the reference address.");
                    gd2.batteryLife();
                    System.out.println("Device "+i+": "+gd2.batteryLife());
                    System.out.println("Device "+i+": "+gd2.toString());
                    ed.add(gd2);
                    i++;
                }
        
                   else if(a.equalsIgnoreCase("android device"))
                    {
                    String cs2 = sc.nextLine();
                    double cst2=Double.parseDouble(cs2);
                    String Date2 = sc.nextLine();
                    DateFormat d2 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result2 =  d2.parse(Date2); 
                    String make2 = sc.nextLine();
                    String bty1 = sc.nextLine();
                    double btry1 = Double.parseDouble(bty1);
                    String scr1 = sc.nextLine();
                    double scrnsz1 = Double.parseDouble(scr1);
                    String vsn = sc.nextLine();
                    double vrsn = Double.parseDouble(vsn);
                    ElectronicDevices ad = new AndroidDevices(vrsn,btry1,scrnsz1,cst2,result2,make2);
                    AndroidDevices ad1 = (AndroidDevices)ad;
                    System.out.println("Device "+i+": "+ad1.getVersion());;
                    System.out.println("Device "+i+": "+ad1.batteryLife());
                    System.out.println("Device "+i+": "+ad1.expensiveOrNot());
                    System.out.println("c. No,there is no chance of multiple inheritance in java.C++ language supports the multiple inheritance.");
                    System.out.println("Device "+i+": "+ad1.toString());
                    ed.add(ad1);
                    i++;
                    }
                   
                    else if(a.equalsIgnoreCase("IOS device"))
                    {
                    String cs3 = sc.nextLine();
                    double cst3=Double.parseDouble(cs3);
                    String Date3 = sc.nextLine();
                    DateFormat d3 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result3 =  d3.parse(Date3); 
                    String make3 = sc.nextLine();
                    String bty2 = sc.nextLine();
                    double btry2 = Double.parseDouble(bty2);
                    String scr2 = sc.nextLine();
                    double scrnsz2 = Double.parseDouble(scr2);
                    String vrsn1 = sc.nextLine();
                    Boolean id = Boolean.parseBoolean(vrsn1);
                    ElectronicDevices ios = new IOSDevices(id,btry2,scrnsz2,cst3,result3,make3);
                    IOSDevices ios1 = (IOSDevices)ios;
                    System.out.println("Device "+i+": "+ios1.expensiveOrNot());
                    System.out.println("Device "+i+": "+ios1.getModelName());
                    System.out.println("Device "+i+": "+ios1.toString());
                    ed.add(ios1);
                    i++;
                            }
                  
                   else if(a.equalsIgnoreCase("household device"))
                    {
                    String cs4 = sc.nextLine();
                    double cst4=Double.parseDouble(cs4);
                    String Date4 = sc.nextLine();
                    DateFormat d4 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result4 =  d4.parse(Date4); 
                    String make4 = sc.nextLine();
                    String wt = sc.nextLine();
                    double wts = Double.parseDouble(wt);
                    String hei = sc.nextLine();
                    double h = Double.parseDouble(hei);
                    String wid = sc.nextLine();
                    double w = Double.parseDouble(wid);
                    String len = sc.nextLine();
                    double l = Double.parseDouble(len);
                    ElectronicDevices hhd = new HouseholdDevices(wts,h,w,l,cst4,result4,make4);
                    System.out.println("Device "+i+": "+hhd.toString());
                    ed.add(hhd);
                    i++;
                    }
               
                   else if(a.equalsIgnoreCase("oven"))
                    {
                    String cs5 = sc.nextLine();
                    double cst5=Double.parseDouble(cs5);
                    String Date5 = sc.nextLine();
                    DateFormat d5 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result5 =  d5.parse(Date5); 
                    String make5 = sc.nextLine();
                    String wt1 = sc.nextLine();
                    double wts1 = Double.parseDouble(wt1);
                    String hei1 = sc.nextLine();
                    double h1 = Double.parseDouble(hei1);
                    String wid1 = sc.nextLine();
                    double w1 = Double.parseDouble(wid1);
                    String len1 = sc.nextLine();
                    double l1 = Double.parseDouble(len1);
                    String ty = sc.nextLine();
                    ElectronicDevices ov = new Oven(ty,wts1,h1,w1,l1,cst5,result5,make5);
                    Oven ove = (Oven)ov;
                    System.out.println("Device "+i+": "+ove.getDeviceArea());
                    System.out.println("Device "+i+": "+ove.expensiveOrNot());
                    System.out.println("Device "+i+": "+ove.getWatts());
                    System.out.println("Device "+i+": "+ove.powerUsedInKWs(20));
                    System.out.println("Device "+i+": "+ove.toString());
                    ed.add(ove);
                    i++;
                    }
                    else if (a.equalsIgnoreCase("washing machine"))
                    {
                    String cs6 = sc.nextLine();
                    double cst6=Double.parseDouble(cs6);
                    String Date6 = sc.nextLine();
                    DateFormat d6 = new SimpleDateFormat("MM-dd-yyyy",Locale.ENGLISH);
                    Date result6 =  d6.parse(Date6); 
                    String make6 = sc.nextLine();
                    String wt2 = sc.nextLine();
                    double wts2 = Double.parseDouble(wt2);
                    String hei2 = sc.nextLine();
                    double h2 = Double.parseDouble(hei2);
                    String wid2 = sc.nextLine();
                    double w2 = Double.parseDouble(wid2);
                    String len2 = sc.nextLine();
                    double l2 = Double.parseDouble(len2);
                    String ty1 = sc.nextLine();
                    ElectronicDevices wm = new WashingMachine(ty1,wts2,h2,w2,l2,cst6,result6,make6);
                    System.out.println("Device "+i+": "+wm.toString());
                    ed.add(wm);
                    i++;
                    }
        
        }
            for(ElectronicDevices e: ed)
             {
                System.out.println(e);
                    
             }
    
    }
}
                    
                        
                    
                    
                    
                    
                    
                    
                
                
                
                
               
                
            
        
    
    

